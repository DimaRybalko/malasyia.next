import Image from "next/image";
import styles from "../../styles/hero.css";
export default function Hero(params) {
  return (
    <>
      <div className="hero">
        <Image
          className="hero__img"
          src="/swiper1.jpg"
          width={1000}
          height={1000}
          alt="image"
        />
        <div className="hero__text">
          <h2 className="swiper__tittle">
            Trusted Coated Steel Sheet Manufacturer
          </h2>
          <p className="swiper__text">
            It is a Malaysian large private enterprise mainly engaged in the
            production, R&D and sales of color coated sheet. It has passed the
            international standard certification of ISO9000: 2008 Quality
            Management System.
          </p>
        </div>
      </div>
    </>
  );
}
