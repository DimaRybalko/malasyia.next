import Image from "next/image";
export default function CatalogueCard({ imgSrc, name, alt }) {
  return (
    <div className="product__wrapper">
      <div className="product__img">
        <Image
          className="img"
          src={imgSrc[0]}
          alt={alt}
          width={500}
          height={500}
        />
      </div>
      <div className="product__text__wrapper">
        <p className="product__text">{name}</p>
      </div>
      <div className="product__button__wrapper">
        <button className="product__button">Open description</button>
      </div>
    </div>
  );
}
