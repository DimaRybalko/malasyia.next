"use client";
import Link from "next/link";
import CatalogueCard from "./cataloguecard";
import { useEffect, useState } from "react";
import styles from "../../styles/catalogue.scss";

export default function Catalogue() {
  const [products, setProducts] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("data.json");
        const result = await response.json();
        setProducts(result);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, []);
  return (
    <>
      <h2 className="catalogue__tittle">Our products</h2>
      <div className="catalogue__products">
        {products.map(({ imgSrc, name, id }) => (
          <Link href={`/cardpage/${id}`} key={id}>
            <CatalogueCard imgSrc={imgSrc} name={name} alt={name} />
          </Link>
        ))}
      </div>
    </>
  );
}
