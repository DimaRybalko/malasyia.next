"use client";
import Link from "next/link";
import styles from "../../styles/header.css";
import Image from "next/image";
export default function Header() {
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <header className="">
      <div className="header__container">
        <div className="header__logo">
          <Link href="/" legacyBehavior={true}>
            <a>
              <Image
                className="logo"
                src="/logo.png"
                width={200}
                height={200}
                alt="image"
              />
            </a>
          </Link>
        </div>
      </div>
    </header>
  );
}
