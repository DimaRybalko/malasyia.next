import styles from "../../styles/footer.css";
export default function Footer(params) {
  return (
    <footer>
      <div className="footer__container ">
        <div className="footer__content container">
          <div className="footer__item">
            <h3 className="item__tittle">Products</h3>
            <div>
              <p className="item__text">Prepainted Steel Coils</p>
            </div>
          </div>
          <div className="footer__item">
            <h3 className="item__tittle">Contact Us</h3>
            <div className="item__text__container">
              <p className="item__text">
                Tel/Fax:{" "}
                <a className="footer__a" href="tel:+603-22645295">
                  +603-22645295
                </a>
              </p>
              <p className="item__text">
                E-mail:{" "}
                <a className="footer__a" href="mailto:maxchin@igtrade.com.my">
                  maxchin@igtrade.com.my
                </a>
              </p>
            </div>
          </div>
          <div className="footer__item">
            <h3 className="item__tittle">Address</h3>
            <div className="item__text__container">
              <p className="item__text">
                NO,7-1.IST FLOOR, <br />
                LORONG BATU NILAM 34B.BANDAR BUKIT TINGGI,
                <br /> KLANG 41200 SELANGOR DARUL EHSAN, MALAYSIA
              </p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
