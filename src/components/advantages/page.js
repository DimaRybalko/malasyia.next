import AdvantagesCard from "./advantagesCard";
import styles from "../../styles/advantages.css";
export default function Advantages(params) {
  return (
    <div className="advantages__container background__container">
      <div className="advantages__content">
        <h2 className="advantages__tittle">Advantages</h2>
        <p className="advantages__text">
          At present, INGENUITY TRADE SDN BHD has got three galvanizing and
          aluminum-zinc plating production lines, three color coated steel plate
          production lines.
        </p>
      </div>
      <div className="advantages__offers">
        <AdvantagesCard
          className="offer__image"
          tittle={"Professional Certification"}
          text={
            "Passed the international ISO9000:2008 quality management system standard certification enterprise"
          }
          img="/diamond.png"
        />
        <AdvantagesCard
          className="offer__image modified__card"
          tittle={"Quality Assurance"}
          text={
            "It was recognized by the Provincial Bureau of Quality and Technical Supervision"
          }
          img="/symbol.jpg"
        />
        <AdvantagesCard
          className="offer__image "
          tittle={"Quality Inspection"}
          text={
            "Professional quality inspection team and professional equipment to provide you with professional product protection."
          }
          img="/steel.png"
        />
        <AdvantagesCard
          className="offer__image"
          tittle={"Professional Laboratory"}
          text={
            "The company has a modern office environment and facilities, as well as large-scale advanced  qulity controll equipment"
          }
          img="/team.png"
        />
      </div>
    </div>
  );
}
