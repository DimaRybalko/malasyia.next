import Image from "next/image";
export default function AdvantagesCard({ tittle, text, img, className }) {
  return (
    <div className="offer__card">
      <div className="offer__img">
        <Image
          src={img}
          width={500}
          height={500}
          alt="image"
          className={className}
        />
      </div>
      <div className="offer__signs">
        <h3 className="offer__tittle">{tittle}</h3>
        <p className="offer__text">{text}</p>
      </div>
    </div>
  );
}
