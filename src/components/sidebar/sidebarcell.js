import Image from "next/image";
import { useState } from "react";

export default function Sidebarcell({ img }) {
  const [isHovered, setIsHovered] = useState(false);
  return (
    <>
      <div
        className="sidebarcell"
        onMouseEnter={() => setIsHovered(true)}
        onMouseLeave={() => setIsHovered(false)}
      >
        <Image
          className="sidebar__img"
          src={img}
          alt=""
          width={500}
          height={500}
        />
        {isHovered && (
          <div className="hiddenContent">
            <a href="tel:+603-22645295">+603-22645295</a>
          </div>
        )}
      </div>
    </>
  );
}
