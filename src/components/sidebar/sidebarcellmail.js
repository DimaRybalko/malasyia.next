import Image from "next/image";
import { useState } from "react";

export default function Sidebarcellmail({ img }) {
  const [isHovered, setIsHovered] = useState(false);
  return (
    <>
      <div
        className="sidebarcell"
        onMouseEnter={() => setIsHovered(true)}
        onMouseLeave={() => setIsHovered(false)}
      >
        <Image
          className="sidebar__img"
          src={img}
          alt=""
          width={500}
          height={500}
        />
        {isHovered && (
          <div className="hiddenContentmail">
            <a href="mailto:maxchin@igtrade.com.my">maxchin@igtrade.com.my</a>
          </div>
        )}
      </div>
    </>
  );
}
