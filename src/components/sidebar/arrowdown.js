export default function Arrowdown(params) {
  const scrollToBottom = () => {
    window.scrollTo({
      top: document.documentElement.scrollHeight,
      behavior: "smooth",
    });
  };

  return (
    <div className="arrow-container" onClick={scrollToBottom}>
      &#9660;
    </div>
  );
}
