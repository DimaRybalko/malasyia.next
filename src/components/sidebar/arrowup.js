import { useEffect, useState } from "react";

export default function Arrowup(params) {
  const [State, setState] = useState(false);
  useEffect(() => {
    const handleScroll = () => {
      const cameraY = window.scrollY;
      if (cameraY > 100) {
        setState(true);
      } else {
        setState(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <div
      className={State ? "arrow-container" : "arrow-container inactive"}
      onClick={scrollToTop}
    >
      &#9650;
    </div>
  );
}
