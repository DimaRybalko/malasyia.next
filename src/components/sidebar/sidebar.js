"use client";
import Arrowup from "./arrowup";
import Sidebarcell from "./sidebarcell";
import Sidebarcellmail from "./sidebarcellmail";
import styles from "../../styles/sidebar.css";
import Arrowdown from "./arrowdown";

export default function Sidebar(params) {
  return (
    <div className="sidebar">
      <Arrowup />
      <Sidebarcell img={"/phone.png"} />
      <Sidebarcellmail img={"/post.png"} />
      <Arrowdown />
    </div>
  );
}
