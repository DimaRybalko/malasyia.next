import Image from "next/image";

export default function About() {
  return (
    <div className="about__container">
      <div className="about__content">
        <div className="about__content__text">
          <h2 className="about__tittle">About Us</h2>
          <p className="about__text">
            INGENUITY TRADE SDN BHD is a large-scale Malaysian private
            enterprise mainly engaged in the production, R&D and sales of
            high-end color coated sheet, galvanized sheet and steel building
            materials. The company passed the certification of ISO9001: 2015
            Quality Management System, the certification of ISO14001: 2015
            Environmental Management System and the certification of ISO45001:
            2018 Occupational Health and Safety Management System in the
            industry.
          </p>
        </div>
        <div className="about__image">
          <Image
            className="image"
            src="/aboutimage.jpg"
            width={800}
            height={800}
            alt="image"
          />
        </div>
      </div>
    </div>
  );
}
