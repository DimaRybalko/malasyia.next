export default function TableItem({ item }) {
  return (
    <tr>
      {!Array.isArray(item[1]) ? (
        <>
          <td>{item[0]}</td>
          <td>{item[1]}</td>
        </>
      ) : (
        <>
          <td>{item[0]}</td>
          <td>
            {item[1] && item[1].map((i, index) => <tr key={index}>{i}</tr>)}
          </td>
        </>
      )}
    </tr>
  );
}
