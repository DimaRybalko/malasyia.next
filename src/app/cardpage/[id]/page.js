"use client";
import styles from "../../../styles/cardpage.scss";
import Image from "next/image";
import { useParams } from "next/navigation";
import React, { useEffect, useRef, useState } from "react";
import TableItem from "../tableitem";

export default function Cardpage(params) {
  const { id } = useParams();
  const productNum = id;
  const [product, setProducts] = useState([]);
  const [table, setTable] = useState([]);
  const [activeImg, setActiveImg] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("/data.json");
        const result = await response.json();
        console.log(result);
        const product = result.find(({ id }) => id === productNum);
        const table = Object.entries(product.table);
        setProducts(product);
        setTable(table);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, []);
  const handleImgIndex = (index) => setActiveImg(index);

  return (
    <div className="product__card container">
      <div className="main__info">
        <div className="img__wrapper">
          <Image
            quality={100}
            className="img"
            src={product.imgSrc && product.imgSrc[activeImg]}
            alt={product.name}
            width={500}
            height={500}
          />
          <div className="little__icons">
            {product.imgSrc &&
              product.imgSrc.map((icon, index) => (
                <Image
                  quality={100}
                  className="icon"
                  key={index}
                  alt={index}
                  src={icon}
                  onClick={() => handleImgIndex(index)}
                  width={500}
                  height={500}
                />
              ))}
          </div>
        </div>
        <div className="main__info__wrapper">
          <p className="name">{product.name}</p>
          <hr />
          <p className="category">
            Category: <span className="category-bold">{product.category}</span>
          </p>
          <hr />
          <p className="short__description">{product.shortDescription}</p>
          <hr />
        </div>
      </div>
      <div className="details__info">
        <p className="details">Details</p>
        <p>{product.description}</p>
        <div className="additional__images">
          <Image
            quality={100}
            className="additional__image"
            alt={"photo"}
            src={"/colors.png"}
            width={500}
            height={500}
          />
          <Image
            quality={100}
            className="additional__image"
            alt={"photo"}
            src={"/PPGI.png"}
            width={500}
            height={500}
          />
        </div>
        <table id="customers">
          <thead>
            <th>Property</th>
            <th>Information</th>
          </thead>
          <tbody>
            {table &&
              table.map((item, index) => <TableItem key={index} item={item} />)}
          </tbody>
        </table>
      </div>
    </div>
  );
}
