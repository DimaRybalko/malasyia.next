import About from "@/components/About/About";
import styles from "../styles/about.css";
import Advantages from "@/components/advantages/page";
import Hero from "@/components/hero/page";
import Catalogue from "@/components/catalogue/page";

export default function Home() {
  return (
    <>
      <section>
        <Hero />
      </section>
      <section>
        <div className="container">
          <About />
        </div>
      </section>
      <section>
        <div className="container">
          <Catalogue />
        </div>
      </section>
      <section>
        <Advantages />
      </section>
    </>
  );
}
