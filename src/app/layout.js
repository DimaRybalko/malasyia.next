import Header from "@/components/header/page";
import "./globals.css";
import { Roboto } from "next/font/google";
import Footer from "@/components/footer/page";
import Sidebar from "@/components/sidebar/sidebar";

const roboto = Roboto({
  weight: "400",
  subsets: ["latin"],
  display: "swap",
});

export const metadata = {
  title: "INGENUITY TRADE SDN BHD ( Malaysia)",
  description:
    "Production of prepainted  galvanized steel coils (PPGI) in Malaysia; Production of galvanized steel coils (GI) in Malaysia;Production of Сolor coated steel coils in Malaysia",
  keywords:
    "coils , Malasiya, galvanized steel,Сolor coated steel coils, INGENUITY TRADE ",
  openGraph: {
    title: "INGENUITY TRADE SDN BHD ( Malaysia)",
    description:
      "Production of prepainted  galvanized steel coils (PPGI) in Malaysia;",
    image:
      "https://aybametal.com/storage/app/media/Galvaniz-Rulo-Sac/galvaniz-rulo-sac.webp",
  },
};
export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={roboto.className}>
        <Header />
        <Sidebar />
        <main>{children}</main>
        <Footer />
      </body>
    </html>
  );
}
